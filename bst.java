import java.io.*;

class bst {
    class Node  
    { 
        int key; 
        Node left, right; 
  
        public Node(int item)  
        { 
            key = item; 
            left = right = null; 
        } 
    } 

    Node root;

    bst(){
        root = null;
    }

    void insert(Node root, int key){
        if(root = null){
            root = new Node(key);
            return root;
        }

        if(key < root.key){
            root.left = insert(root.left, key);
        }
        else if(key > root.key){
            root.right = insert(root.right, key);
        }

        return root;
    }

    void traverse(Node root){
        if(root != null){
            traverse(root.left);
            traverse(root.right);
        }
    }

    public static void main(String[] args)  {
        bst bstOb = new bst();
        insert(5);
        insert(8);
        insert(7);
        insert(2);
        traverse(bst.root);
    }
}